<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Comment extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'		=> [
				'type' 				=> 'INT',
				'constraint'		=> 5,
				'unsigned'			=> true,
				'auto_increment'	=> true,
				'null'				=> false
			],
			'postid'		=> [
				'type' 				=> 'INT',
				'constraint'		=> 5,
				'unsigned'			=> true,
				'null'				=> false
			],
			'name'			=> [
				'type'				=> 'TEXT',
				'null'				=> false
			],
			'email'		=> [
				'type'				=> 'TEXT',
				'null'				=> false
			],
			'body'		=> [
				'type'				=> 'TEXT',
				'null'				=> false
			]
		]);
		$this->forge->addKey('id');
		$this->forge->addForeignKey('postid', 'post', 'id', 'CASCADE', 'CASCADE');
		$this->forge->createTable('komentar');
	}

	public function down()
	{
		$this->forge->dropTable('komentar');
	}
}
